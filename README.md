We will lay out our proposal and development plans. Here we will list our MVP for the project.

## The Foundations For: A Post Covid Dynamic Social Gaming Experience

# Team Name Options:
1. Team Nerf Herders
2. The Cool Kids
3. SpaceCrew
4. SpaceZone
5. Distance Space
6. Safe Distance
7. Socially Distance
8. Distant Space
9. Socially Distant
10. Invaders of the Introverts
11. CommonSpace
12. CloseSpace
13. Space Force

## Resources
1. Mind-Mapping: https://miro.com/app/board/o9J_kmE4P-8=/
2. Trello: https://trello.com/invite/b/D9HEHoUw/04602d770030618971c13a066ddca932/kenzie-capstone
3. Repo: https://gitlab.com/MissTitanK3/apdsge
4. Google Doc: https://docs.google.com/document/d/1ZV-2xPURkbwX1pR4kNMERejzQn-ity2eL0g0r32ZYog/edit?usp=sharing
5. Google Shared Folder: https://drive.google.com/drive/folders/1A3p682WgQgw0FhKvOlUFBdjGwxoSSYn9?usp=sharing
6. Heroku Deployment: https://apdsge.herokuapp.com
7. Capstone Feedback Form: https://docs.google.com/forms/d/1yeIyQH6ZE6y5Z0qB2i8yW5_1Gzfxs8YiJsNlcyjR0WA/viewform?edit_requested=true
8. Retrospective Document: https://docs.google.com/document/d/1GrolzlkNK1OQEpFT5gwpZmMc2QHwCOZmIFNV7WMap4g/edit?usp=sharing

Today we will be talking about how people desire to get out of their homes and be safe while being out and about. This
concept will allow an opportunity to do just that. This combines a game that people can participate in and social
distancing. The reason why this will encourage social distancing is because there is a zone of effective triggered
events (ZETE). Participants must maintain 6 feet or else their ship (Virtual Environment) will start to take damage. And
if they are beyond 10 feet then they will be ineffective in any interaction. The overall MVP for this project is to
produce a User interface, registration and persistent database. Now let's get into the details.

    The ratio of complexity is fairly straight forward. This project seems to be pretty balanced with a 50/50 ratio of frontend and backend development. The tools that will be used are React, Mongoose/MongoDB, Express, Netlify and Node. Depending on my team, we will either use a UI/CSS library or build our own. I will leave that decision for the team once they are assembled.

    I have broken down my timeline into 7 stages of development. Each stage will need to be completed before moving onto the next. The first stage is the Redline Items. These are the foundations of the project. The Redline Items include setting up the database that will be used and the react front end. From there we have the Orangeline Items. Once we get here the focus turns to components that will be assigned to different team members. These include the register, login, character profile, and remove account in the front end. This will also include the ambient browser environment, the Bridge of a Starship. Then on the backend we will set up the endpoints for the database. This satisfies most of the requirements but not all. From here we have the Yellowline Items. This is where the requirements will be met. You will notice that up until now the objectives have all been in green, and now we have orange ones. What are those, you may ask. I am here to tell you. Items that are orange are features that can be included, but won't break the experience if they are not there. These are advanced features, if you will. But let us get back to it. In this level we will build out the rest of the functionality for the program. We will have User fields for Name and location, a template for the stations and confirmation that the database is working properly.

    There is a lot more going on tho, right? Well, let me tell you about that. I intend to develop this into a market ready product. And these are the items that will be needed for a full beta release. You will have access to check out the Mind-Mapping that was developed for this project. Items that are highlighted in yellow are the features that will take longer to build that we have time for. Same goes for the red. But I have included these, with their perspective line items representation so that I can develop beyond the submission of the assignment.

## Branch Structure
initials-feature

## Investor
#### The Bank
- Name: Vince St. Loius
- Timezone: Eastern
- Eamil: vincent.stlouis@kenzie.academy
- Meeting Link: N/A

#### Main Contact
- Name: Surivita Roy
- Timezone: Eastern
- Eamil: suravitaroy@gmail.com
- Meeting Link: https://calendly.com/suravita/

### Investor Questions, Comments, Concerns

None
## Team

|      Name         |Scrum Role             |I worked on               |Struggles               |
|----------------|-------------------------------|-----------------------------|-----------------------------|
|Suri|`Product Investor`|Investing|Fill me out...|
|David Fidler|`QA`|Fill me out...|
|Tianna McCoy|`Product Owner`| General|Fill me out...|
|Myagmarsuren Otgonjargal|`Scrum Master`| Fill me out...|Fill me out...|
|David Fidler|`Dev`| BACKEND|Fill me out...|
|Joaquin L Lopez|`Dev`| BACKEND LEAD|Fill me out...|
|Moe Logins|`Dev`| FRONTEND LEAD|Fill me out...|
|Myagmarsuren Otgonjargal|`Dev`| FRONTEND|Fill me out...|

## Team Analysis:

1. Tianna

- Strengths
  - HTML
  - CSS
  - MERN
    - Mongo
    - Express
    - React
    - Node
- Weaknesses
  - Communications
  - Git
  - Tech Support
- Opportunities
  - communicating better
  - Following up on learning Git further
  - Seeing and solving more errors
- Availability
  - Open mostly.
  - Except for study halls

2. Moe

- Strengths
  - HTML/CSS
  - JS
  - Redux
- Weaknesses
  - GIT
  - Back-end (currently)
- Opportunities
  - Understanding back-end
  - Working on API/Get (post, put, etc)
- Availability
  - (Mon-Sat) 9am-3pm EST

3. Joaquin

- Strengths
  - React/JS
  - Backend
- Weaknesses
  - styling
- Opportunities
- Availability
  - Varies week to week but typically 4 full days a week (school-day + night)

4. Myagmarsuran

- Strengths
  - Front End
  - CSS
  - HTML
- Weaknesses
  - React
  - API
- Opportunities
- Availability
  - Free from 9-5

5. David

- Strengths
  - QA
  - Git
  - Tech Support
- Weaknesses
  - React
  - MongoDB
- Opportunities
  - Obtaining a stronger understanding of React
- Availability
  - Open

**!! Commits everyday by 12:00pm Estern with updates. Even if you didn't produce any code. (Documenting that you got
stuck on a section) !!**

## MVP (Minimum Viable Product)

- USE REACT FOR THE ENTIRE FRONTEND
- PERSIST ALL DATA TO A DATABASE OF YOUR CHOOSING (NOT THE FILE SYSTEM)
- DEPLOY YOUR APP
- USE A STATE MANAGEMENT SYSTEM FOR YOUR FRONTEND (MUST PERSIST AUTH STATE ON REFRESHES)
- USE NODEJS FOR ALL OF THE BACKEND
- MUST INCLUDE API AUTHENTICATION (LOGIN / LOGOUT)
- MUST INCLUDE AT A MINIMUM 10 API REQUESTS
- SCRUM BOARD [TRELLO OR SOMETHING SIMILAR] - Your board must accurately reflect the projects goal by using tasks and
  assigning users to those tasks
- DEMO PARTICIPATION
- APPROVED ADVANCED FEATURE
- TEAM PARTICIPATION [Git commit history accurately reflects what you worked on in your README.MD ] - Commits must be
  proportional to what your other group members have committed as well.
  - 20.0 pts
    - Full Marks
  - 15.0 pts
    - Good commit history that shows you made a great contribution
  - 10.0 pts
    - Mediocre commit history that shows you had a fair history
  - 5.0 pts
    - Poor commit history that shows you did not contribute much to the project via code. Concerned about how meaningful
      your history was.
  - 0.0 pts
    - You did not commit or your commits did not contribute anything meaningful to the project
- TEAM PARTICIPATION [Cumulative Group Feedback Form]
  - 20.0 pts
    - Full Marks
    - Received AMAZING feedback
  - 15.0 pts
    - Received mostly positive feedback
  - 10.0 pts
    - Received ok feedback
  - 5.0 pts
    - Received mostly negative feedback
  - 0.0 pts
    - You did not do work well with your team
- PROJECT COMPLETION [A WORKING product that accurately reflects the pitch]
- README.MD FULLY FILLED OUT

### Development Plan for APDSGE

## Component Tree / Component Structure

- [ ] API
  - [ ] Axios
- [ ] Component
  - [ ] Settings
    - [ ] Music
    - [ ] Sound Effects
    - [ ] Not Darkmode (!)
    - [ ] Timeline Theme
      - [ ] Basic Timeline Themes
      - [ ] Premium Timeline Themes (!)
  - [ ] Home/ Landing
    - [ ] Information about the app
    - [ ] Features
    - [ ] Creators
    - [ ] Contact us / Feedback
    - [ ] FAQ / Help
  - [ ] Navigation
    - [ ] Responsive
      - [ ] Hamburger menu for mobile
      - [ ] Top Nav for Desktop
    - [ ] Icons (Favicon)
  - [ ] Register
    - [ ] Gravatar
    - [ ] Google Login (!)
  - [ ] Login
    - [ ] Jasonwebtoken
  - [ ] Character
    - [ ] Create
      - [ ] Name
      - [ ] Location
      - [ ] Uniforms (!)
      - [ ] Badge Creator (!)
      - [ ] Age (!)
      - [ ] Group (!)
      - [ ] Messages (!)
      - [ ] Create Button (S)
    - [ ] Update Character
      - [ ] Update Button (S)
      - [ ] Delete Character
    - [ ] 4 Delete confirmations
      - [ ] Delete Button (S)
  - [ ] Bridge
    - [ ] Stations template
    - [ ] Create each station
  - [ ] Img
  - [ ] GitIgnore
    - [ ] BE SURE TO IGNORE ESLINTCACHE from the start.
    - [ ] node_module/
  - [ ] Styles.js
  - [ ] Timeline Theme
    - [ ] Basic Timeline Themes
    - [ ] Premium Timeline Themes

## Application State (Data)

- [ ] MongoDB Atlas
  - [ ] Post
    - [ ] Creating the account
    - [ ] Creating the character
  - [ ] Get
    - [ ] Getting the User Information
    - [ ] Displaying it
    - [ ] Entering the Bridge
    - [ ] Protected Route
  - [ ] Patch / Put
    - [ ] Updating Account profile
    - [ ] Update Individual Characters
    - [ ] Update the Stations that characters are assigned to
  - [ ] Delete
    - [ ] Delete a Character
    - [ ] Delete account

## Components vs Containers

- Styled Components
- Functional Components (Hooks) prefered over class components
- Deployment to Heroku

## Source Center
Update any place you referenced and implimented code from them.

- Name of source
  - LINK
