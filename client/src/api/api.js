import axios from "axios";

const URL = "http://localhost:4000";

const axiosInstance = axios.create({
  baseURL: URL,
});
// eslint-disable-next-line
const authInstance = axios.create({
  baseURL: URL,
  headers: {
    auth: `Bearer ${localStorage.getItem("token")}`,
  },
});

export const createUser = async (Username, Email, Password) => {
  console.log(Username, Email, Password);
  try {
    const { data } = await axiosInstance.post("/user/register", {
      username: Username,
      email: Email,
      password: Password,
    });
    return data;
  } catch (err) {
    console.error(err.message);
  }
};

export const loginUser = async (Email, Password) => {
  try {
    const { data } = await axiosInstance.post("/user/login", {
      email: Email,
      password: Password,
    });
    console.log(data);
    return data;
  } catch (err) {
    console.log(err.message);
  }
};

export const getUser = async () => {
  console.log("hello");
  try {
    const jwt = localStorage.getItem("token");
    console.log(jwt);
    const { data } = await axiosInstance.get("/user", {
      headers: {
        auth: `${jwt}`,
      },
    });
    console.log(data);
    return data;
  } catch (err) {
    console.log(err.message);
  }
};

export const newsFeed = async () => {
  try {
    const jwt = localStorage.getItem("token");
    const { data } = await axiosInstance.get("/newsFeed", {
      headers: {
        auth: `${jwt}`,
      },
    });
    return data;
  } catch (err) {
    console.log(err.message);
  }
};

export const updateDesc = async (newDesc) => {
  try {
    const jwt = localStorage.getItem("token");
    const { data } = await axiosInstance.patch(
      "/dashboard",
      { description: newDesc },
      {
        headers: {
          auth: `${jwt}`,
        },
      },
    );
    return data;
  } catch (err) {
    console.log(err.message);
  }
};

export const submitChara = async (color, name) => {
  try {
    const jwt = localStorage.getItem("token");
    const { data } = await axiosInstance.post(
      "/character",
      {
        name: name,
        color: color,
      },
      {
        headers: {
          auth: `${jwt}`,
        },
      },
    );
    console.log(data);
    return data;
  } catch (err) {
    console.log(err.message);
  }
};

export const getChars = async () => {
  try {
    const jwt = localStorage.getItem("token");
    console.log("jwt", jwt);
    const { data } = await axiosInstance.get("/character", {
      headers: {
        auth: `${jwt}`,
      },
    });
    console.log(data, "data");
    return data;
  } catch (err) {
    console.log(err.message);
  }
};

export const getMessages = async () => {
  try {
    const jwt = localStorage.getItem("token");
    console.log("jwt", jwt);
    const { data } = await axiosInstance.get("/messages", {
      headers: {
        auth: `${jwt}`,
      },
    });
    console.log(data, "data");
    return data;
  } catch (err) {
    console.log(err.message);
  }
};

export const postMessage = async (Message) => {
  try {
    const jwt = localStorage.getItem("token");
    const { data } = await axiosInstance.post(
      "/messages",
      {
        messages: Message,
      },
      {
        headers: {
          auth: `${jwt}`,
        },
      },
    );
    return data;
  } catch (err) {
    console.log(err.message);
  }
};
