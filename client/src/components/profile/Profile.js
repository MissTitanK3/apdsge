import React, { useEffect, useState } from "react";
import { Card, Button, Input } from "antd";
import { getUser, updateDesc } from "../../api/api";
import { Newsfeed } from "../newsfeed/Newsfeed";
import Login from "../login/Login";
import "antd/dist/antd.css";
import "./profile.css";
export const Profile = () => {
  const [user, setUser] = useState([]);
  const [edit, setEdit] = useState(false);
  const [description, setDescription] = useState("");
  const [descriptionValue, setDescVal] = useState("");
  const [loggedIn, setLoggedIn] = useState(false);
  useEffect(() => {
    const jwt = localStorage.getItem("token");
    if (jwt) {
      setLoggedIn(true);
      getUserData();
    }
  }, []);

  const getUserData = async () => {
    const data = await getUser();
    console.log(data);
    setUser(data);
    setDescription(data.description);
  };

  const updateDescr = async () => {
    const data = await updateDesc(descriptionValue);
    console.log(data);
  };

  return (
    <>
      {loggedIn ? (
        <>
          <div className="profile_page_container">
            <Card className="header_container">
              <img className="profile_img" src={user.ppic} alt="Profile"></img>
              <h2 className="header_name">{user.username}</h2>
              {edit ? (
                <>
                  <Input.TextArea
                    className="edit_text"
                    type="text"
                    placeholder="New Description"
                    value={descriptionValue}
                    onChange={(e) => {
                      setDescVal(e.target.value);
                    }}
                  />
                  <Button
                    className="save_changes"
                    onClick={(e) => {
                      e.preventDefault();
                      updateDescr();
                      setDescription(descriptionValue);
                      setEdit(false)
                    }}
                  >
                    Save Changes
                  </Button>
                </>
              ) : (
                  <p className="header_description">{description}</p>
                )}
            </Card>
            <Button
              type="primary"
              className="edit_description_btn"
              onClick={() => {
                if (edit) {
                  setEdit(false);
                } else {
                  setEdit(true);
                }
              }}
            >
              Edit Description
            </Button>
          </div>
          <Newsfeed />
        </>
      ) : (
          <Login />
        )}
    </>
  );
};
