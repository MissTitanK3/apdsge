import React, { useState, useEffect } from "react";
import { DashboardNav } from "../dashboard/DashboardNav";
import { newsFeed } from "../../api/api";
import { Card, Col, Row, Modal } from "antd";
import "antd/dist/antd.css";
export const Newsfeed = () => {
  const [news, setNewsFeed] = useState([]);
  const [loggedIn, setLoggin] = useState(false);
  useEffect(() => {
    getNewsData();
    const jwt = localStorage.getItem("token");
    if (jwt) {
      setLoggin(true);
    }
  }, []);

  const getNewsData = async () => {
    const news = await newsFeed();
    setNewsFeed(news);
  };

  return (
    <div className="container">
      <DashboardNav />
      {loggedIn ? (
        news.map((post) => {
          return (
            <div className="card_container">
              <Card title={`${post.title} || ${post.author}`} extra={`${post.date}`}>
                {post.description}
              </Card>
            </div>
          );
        })
      ) : (
        <>
          <div className="login_container">Please Login to view this page</div>
          <div className="fill"></div>
        </>
      )}
    </div>
  );
};
