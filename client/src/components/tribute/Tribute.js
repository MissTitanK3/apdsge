import React from 'react'
import { TributeWrapper } from '../../Styles'

export default function Tribute() {
  return (
    <TributeWrapper>
      <h3>Gene Roddenberry</h3>
      <p>"In a very real sense, we are all aliens on a strange planet. We spend most of our lives reaching out and trying to communicate. If during our whole lifetime, we could reach out and really communicate with just two people, we are indeed very fortunate."</p>
    </TributeWrapper>
  )
}
