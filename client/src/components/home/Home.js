import Tribute from "../tribute/Tribute";
import { HomeWrapper } from "../../Styles"
import About from '../about/About'
import TeamCarousel from "../ourTeam/Carousel";
import { HomeButton } from '../../Styles'
import { useHistory } from "react-router-dom";
// import Background from '../../img/bg_4.jpg'

export default function Home() {
  const history = useHistory()

  const playNow = () => {
    history.push('/register')
  }

  return (
    <>
      <HomeButton onClick={playNow}>PLAY NOW!</HomeButton>
      <HomeWrapper>
        <div id="home">
          <h2>A Post Covid
      <br />
      Dynamic Social Gaming Experience</h2>
          <br />
          <Tribute />
        </div>
      </HomeWrapper>
      <About />
      <div>
        <TeamCarousel />
      </div>
    </>

  )
}