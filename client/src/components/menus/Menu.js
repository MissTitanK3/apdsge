import React from "react";
import GameMenu from "./GameMenu";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import GameHome from "../../gamePages/GameHome";
import Crew from "../../gamePages/Crew";
import ShipStatus from "../../gamePages/ShipStatus";
import Contact from "../contact/Contact";
import { Dashboard } from "../dashboard/Dashboard";
import Login from "../login/Login";
import About from "../about/About";
import Home from "../home/Home";
import Register from "../login/Register";
import Messages from "../messages/Messages";
import Friends from "../friends/Friends";
import Characters from "../characters/Characters";
import { Newsfeed } from "../newsfeed/Newsfeed";
function NoMatch() {
  return (
    <div>
      <h1>404 Not Found</h1>
    </div>
  );
}

function Menu() {
  return (
    //TODO Protected routes
    <>
      <Router>
        <GameMenu />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/newsfeed" component={Newsfeed} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/messages" component={Messages} />
          <Route exact path="/friends" component={Friends} />
          <Route exact path="/characters" component={Characters} />
          <Route path="/game" exact component={GameHome} />

          <Route path="/crew" component={Crew} />
          <Route path="/ship-status" component={ShipStatus} />
          <Route path="*" component={NoMatch} />
        </Switch>
      </Router>
    </>
  );
}

export default Menu;
