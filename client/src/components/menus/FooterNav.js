import { Link } from "react-router-dom";
import { FooterWrapper } from '../../Styles'

export default function FooterNav() {
  return (
    <>
      <FooterWrapper>
        <div id="Footer">
          <ul>
            <li>
              <Link to="/contact">Contact Us</Link>
            </li>
            <li>
              <Link to="/help">FAQ/Help</Link>
            </li>
          </ul>
          <p>
            &copy;{new Date().getFullYear()} CloseSpace | All right reserved | Terms of Service | Privacy
          </p>
        </div>
      </FooterWrapper>
    </>
  );
}
