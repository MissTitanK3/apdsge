import { NavWrapper } from '../../Styles'
import Logo from '../../img/CapstoneLogoV2.png'
import CloseLogo from '../../img/CloseLogo.png'
import SpaceLogo from '../../img/SpaceLogo.png'

export default function Navigation() {
  return (
    <NavWrapper>
      <img id="contCloseLogo" src={CloseLogo} alt="CloseSpace Logo" />
      <img src={Logo} id="contactLogo" alt="CloseSpace Logo" />
      <img id="contSpaceLogo" src={SpaceLogo} alt="CloseSpace Logo" />
    </NavWrapper>
  );
}
