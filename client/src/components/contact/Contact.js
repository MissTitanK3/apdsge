import React from 'react'
import { ContactWrapper } from "../../Styles";
import Logo from '../../img/CapstoneLogoV2.png'
import CloseLogo from '../../img/CloseLogo.png'
import SpaceLogo from '../../img/SpaceLogo.png'


export default function Contact() {
    return (
        <ContactWrapper>
            <div id="contactLogoContainer">
                <img id="contCloseLogo" src={CloseLogo} alt="CloseSpace Logo" />
                <img id="contactLogo" src={Logo} alt="CloseSpace Logo" />
                <img id="contSpaceLogo" src={SpaceLogo} alt="CloseSpace Logo" />
            </div>
            <div id="pageContainer">
                <div id="contactContainer">
                    <div id="contactForm" >
                        <form >
                            <label>
                                <input type="text" name="name" placeholder="Name"/>
                                <input type="email" name="email" placeholder="Email"/>
                                <textarea id="contactTextArea" name="Message" rows="5" cols="45" placeholder="Enter Your Message"></textarea>
                            </label>
                            <input type="submit" value="Submit" />
                        </form>
                    </div>
                    <div id="contactInfo">
                        <h4 id="address">1234 StreetName St, Los Angeles, California </h4>
                        <h4 id="phoneNumber">703-555-5437</h4>
                        <h4 id="contactEmail">kenzie@closespace.com</h4>
                    </div>
                </div>
            </div>

        </ContactWrapper>
    )
}