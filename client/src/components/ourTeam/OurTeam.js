import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { OurTeamWrapper } from "../../Styles";


const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function SimpleCard({ img, name, title, position, bio }) {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <OurTeamWrapper>
      <div >
        <section>
          <h5>
            {title}
          </h5>
          <h3 >
            {name}
          </h3>
          <h5 >
            {position}
          </h5>
          <p>
            {bull}{bio}
          </p>
          <img src={img} alt="teamphoto" />
        </section>
      </div>
    </OurTeamWrapper>
  );
}
