import React, { Component } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import OurTeam from './OurTeam'
import TiannaPhoto from '../img/Tianna.png'
import MoePhoto from '../img/Moe.png'
import JoaquinPhoto from '../img/Joaquin.png'
import MigaPhoto from '../img/Miga.png'
import DavidPhoto from '../img/David.png'

export default class TeamCarousel extends Component {
  render() {
    return (
      <Carousel showThumbs={false}>
        <div>
          <OurTeam img={TiannaPhoto} name='Tianna McCoy' title='Product Owner' position='SE July 2020' bio='Does Pineapple go on pizza?' />
        </div>
        <div>
          <OurTeam img={MoePhoto} name='Moe Logins' title='Client Lead Engineer' position='SE July 2020' bio='Does Pineapple go on pizza?' />
        </div>
        <div>
          <OurTeam img={JoaquinPhoto} name='Joaquin Lopez' title='Server Lead Engineer' position='SE July 2020' bio='Does Pineapple go on pizza?' />
        </div>
        <div>
          <OurTeam img={MigaPhoto} name='Myagmarsuren Otgonjargal' title='Scrum Master' position='SE July 2020' bio='Does Pineapple go on pizza?' />
        </div>
        <div>
          <OurTeam img={DavidPhoto} name='David Fidler' title='Quality Assurance' position='SE July 2020' bio='Does Pineapple go on pizza?' />
        </div>
      </Carousel>
    );
  }
}
