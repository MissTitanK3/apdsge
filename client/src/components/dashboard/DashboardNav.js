import React from "react";
import { DashboardWrapper } from "../../Styles";
import { Tabs } from "react-tabs";
import { Link } from "react-router-dom";
import { HomeOutlined } from "@ant-design/icons";
export const DashboardNav = () => {
  return (
    <DashboardWrapper>
      <Tabs>
        <div id="tabPanel">
          <ul>
            <li>
              <Link to="/Dashboard">
                <div>
                  Dashboard <HomeOutlined />
                </div>
              </Link>
            </li>
            <li>
              <Link to="/messages">
                <div>Messages</div>
              </Link>
            </li>
            <li>
              <Link to="/friends">
                <div>Friends</div>
              </Link>
            </li>
            <li>
              <Link to="/characters">
                <div>Characters</div>
              </Link>
            </li>
          </ul>
        </div>
      </Tabs>
    </DashboardWrapper>
  );
};
