import React from "react";
import { FriendsWrapper } from "../../Styles";
//import { Link } from "react-router-dom";
import { DashboardNav } from "../dashboard/DashboardNav";

export default function Friends() {
  return (
    <FriendsWrapper>
      <DashboardNav />
      <div id="friendsTab">
        <div id="friendsContainer">
          <form id="friendsSearchBar">
            <input type="text" name="search" placeholder="Search.." />
          </form>
          <div id="friends">
            <ul id="friendsList">
              <li>Friend</li>
            </ul>
          </div>
        </div>
      </div>
    </FriendsWrapper>
  );
}
