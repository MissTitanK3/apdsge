import React, { Component } from "react";
import { DashboardNav } from "../dashboard/DashboardNav";
class CharacterForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false,
      formData: {
        characterName: "",
        location: "",
      },
    };
  }

  handleChange = (event) => {
    const formData = { ...this.state.formData };
    formData[event.target.name] = event.target.value;

    this.setState({ formData });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    this.setState({
      submitted: true,
    });
  };

  resetForm = (event) => {
    this.setState({
      submitted: false,
      formData: {
        characterName: "",
        location: "",
      },
    });
  };

  render() {
    if (this.state.submitted) {
      return (
        <>
          <div className="character">
            <p>{this.state.formData.characterName}, you're now ready to play!</p>
            <button onClick={this.resetForm}>Click here to start</button>
          </div>
        </>
      );
    }

    return (
      <>
        <div className="character">
          <form onSubmit={this.handleSubmit}>
            <div>
              <label htmlFor="characterName">Create A Character Name</label>
              <input
                type="text"
                name="characterName"
                value={this.state.formData.characterName}
                onChange={this.handleChange}
              />
            </div>

            <div>
              <label htmlFor="location">Enter Your Location</label>
              <input type="text" name="location" value={this.state.formData.location} onChange={this.handleChange} />
            </div>

            <button>Create</button>
          </form>

          <div>
            {this.state.formData.characterName}
            <br />
            {this.state.formData.location}
          </div>
        </div>
      </>
    );
  }
}

export default CharacterForm;
