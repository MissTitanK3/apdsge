import React from "react";

export const CharacterComponent = React.forwardRef(({ imgProps = {}, src, ...rest }, ref) => (
  <div ref={ref} {...rest}>
    <img className="uniform" {...imgProps} src={src || imgProps.src} alt="" />
  </div>
));
