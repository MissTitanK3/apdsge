import React, { useState, useEffect } from "react";
//todo ask about this
//import scrollToComponent from "react-scroll-to-component";
import { Form, Input, Button, Card } from "antd";
import "antd/dist/antd.css";
import RedUniform from "../img/character-uniform-red.png";
import BlueUniform from "../img/character-uniform-blue.png";
import OrangeUniform from "../img/character-uniform-orange.png";
import { CharacterComponent } from "./CharactersComponent";
import CharacterForm from "./CharacterForm";
import { DashboardNav } from "../dashboard/DashboardNav";
import { submitChara, getChars } from "../../api/api";
const uniforms = [
  { type: "Red Uniform", src: RedUniform },
  { type: "Blue Uniform", src: BlueUniform },
  { type: "Orange Uniform", src: OrangeUniform },
];

const mappedUniforms = uniforms.reduce(
  (acc, { type, src }) => ({
    ...acc,
    [type]: src,
  }),
  {},
);

const SelectUniform = () => {
  const [selected, setSelected] = React.useState(null);

  const onSelect = (type) => () => {
    setSelected(type);
  };

  return {
    selected,
    onSelect,
  };
};

const Characters = () => {
  const { selected, onSelect } = SelectUniform();
  const [select, setSelected] = useState("");
  const [uniColor, setColor] = useState("");
  const [name, setName] = useState("");
  const [charData, setChars] = useState([]);

  useEffect(() => {
    getCharacters();
  }, []);

  const getCharacters = async () => {
    const data = await getChars();
    setChars(data);
  };
  const submitChar = async (e) => {
    console.log("submit");
    console.log(uniColor, name);
    await submitChara(uniColor, name);
  };
  return (
    <>
      <DashboardNav />
      {charData.map((char) => {
        return (
          <div className="card_container">
            <Card title={`${char.name}`}>
              <p>Fleet: {char.group}</p>
              <p>Uniform color: {char.color}</p>
            </Card>
          </div>
        );
      })}
      <br />
      <div>
        <h1>Create your Character</h1>
        <small>Get started!</small>
        <div>
          <h1>Choose your uniform</h1>
          <br />
          <div className="uniforms">
            {uniforms.map((props) => (
              <CharacterComponent onClick={onSelect(props.type)} isSelected={selected === props.type} {...props} />
            ))}
          </div>
        </div>
        <div className="chosen">
          <h1 className="current_characters">Current Character</h1>
          <div className="selectedUniform">
            <CharacterComponent type={selected} imgProps={{ src: mappedUniforms[selected] }} />
          </div>
          <small>
            You have selected <em>{selected}</em>
          </small>
          <br />
        </div>
        <Form onSubmit={submitChar}>
          <Form.Item label="Uniform Color:">
            <Input
              placeholder="ex. Red"
              value={uniColor}
              onChange={(e) => {
                setColor(e.target.value);
              }}
            />
          </Form.Item>
          <Form.Item label="Character Name:">
            <Input
              placeholder="ex. Starlord"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
          </Form.Item>

          <Button
            type="secondary"
            onClick={() => {
              submitChar();
            }}
          >
            Submit
          </Button>
        </Form>
      </div>
    </>
  );
};

export default Characters;
