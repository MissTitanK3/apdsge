import { AboutWrapper } from "../../Styles";

export default function About() {
  return (
    <>
      <AboutWrapper>
        <div id="about">
          <h1>About CloseSpace</h1>
          <p>
            People desire to get out of their homes and be safe while being out and about. This game will allow an
            opportunity to do just that. This combines a game that people can participate in and social distancing. The
            reason why this will encourage social distancing is because there is a zone of effective triggered events
            (ZETE). Participants must maintain 6 feet or else their ship (Virtual Environment) will start to take damage.
            And if they are beyond 10 feet then they will be ineffective in any interaction.
        </p>
        </div>
        <br />
        <div id="features-right">
          <h1>CloseSpace Features</h1>
          <div>
            <ul>
              <h2>Current Features</h2>
              <li>Create an account</li>
              <li>Create a character</li>
              <li>Messaging</li>
              <li>Bridge tutorial</li>
            </ul>
          </div>
        </div>
        <br />
        <div id="features-left">
          <h1>Coming Soon</h1>
          <div>
            <ul>
              <h2>Future Features</h2>
              <li>Fleets</li>
              <li>Stations</li>
              <li>Google location integration</li>
            </ul>
          </div>
        </div>
        <br />
        <h1 className='rebecca'>CloseSpace Team</h1>
      </AboutWrapper>

    </>
  );
}
