import React, { useState, useEffect } from "react";

import "./messages.css";
import { getMessages, postMessage } from "../../api/api";
import { DashboardNav } from "../dashboard/DashboardNav";
import { Button } from "antd";
import "antd/dist/antd.css";
export default function Messages() {
  const [loggedIn, setLoggedIn] = useState(false);
  const [inputMessage, setInputMess] = useState("");
  const [messages, setMessages] = useState([]);
  useEffect(() => {
    const jwt = localStorage.getItem("token");
    if (jwt) {
      setLoggedIn(true);
      findMessages();
    }
  }, []);

  const findMessages = async () => {
    const data = await getMessages();
    console.log(data, "findMessages");
    setMessages(data);
  };

  return (
    <>
      {loggedIn ? (
        <>
          <DashboardNav />
          <div className="messages_container">
            <div className="inbox_container">
              {messages.map((msg) => {
                console.log(msg);
                return (
                  <>
                    <div className="message">
                      {msg.messages} - {msg.name}
                    </div>
                  </>
                );
              })}
            </div>
            <div className="submit_message">
              <input
                className="input_message"
                type="message"
                value={inputMessage}
                onChange={(e) => {
                  setInputMess(e.target.value);
                }}
              ></input>
              <Button
                className="submit_message_btn"
                type="primary"
                onClick={() => {
                  console.log(inputMessage);
                  postMessage(inputMessage);
                  setInputMess("");
                }}
              >
                Send
              </Button>
            </div>
          </div>
        </>
      ) : (
        <div>Please Log in to view</div>
      )}
    </>
  );
}
