import React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { Link } from "react-router-dom";
import { SettingsMenuData } from "./SettingsMenuData";
import * as IoIcons from "react-icons/io5";
import { IconContext } from "react-icons";
import "./SettingsMenu.css";

export default function SettingsMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <IconContext.Provider value={{ color: "#00a" }}>
        <div>
          <div className="settingsGear">
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
              <IoIcons.IoSettingsSharp />
            </Button>
          </div>
          <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
            <MenuItem onClick={handleClose}>
              <ul>
                {SettingsMenuData.map((item, index) => {
                  return (
                    <li key={index} className={item.cName}>
                      <Link to={item.path}>
                        {item.icon}
                        <span className="settingsItems">{item.title}</span>
                      </Link>
                    </li>
                  );
                })}
              </ul>
            </MenuItem>
          </Menu>
        </div>
      </IconContext.Provider>
    </>
  );
}
