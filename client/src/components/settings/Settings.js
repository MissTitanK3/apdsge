import React from "react";
import SettingsMenu from "./SettingsMenu";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Music from "../../settingsPages/Music";
import Sounds from "../../settingsPages/Sounds";
import Theme from "../../settingsPages/Theme";

function Settings() {
  return (
    <>
      <Router>
        <SettingsMenu />
        <Switch>
          <Route path="/background-music" component={Music} />
          <Route path="/sound-effects" component={Sounds} />
          <Route path="/theme" component={Theme} />
        </Switch>
      </Router>
    </>
  );
}

export default Settings;
