import React from "react";
import * as AiIcons from "react-icons/ai";
import * as MdIcons from "react-icons/md";
import * as SiIcons from "react-icons/si";

export const SettingsMenuData = [
  {
    title: "Background Music",
    path: "/background-music",
    icon: <SiIcons.SiApplemusic />,
    cName: "nav-text",
  },
  {
    title: "Sound Effects",
    path: "/sound-effects",
    icon: <AiIcons.AiFillSound />,
    cName: "nav-text",
  },
  {
    title: "Theme",
    path: "/theme",
    icon: <MdIcons.MdWallpaper />,
    cName: "nav-text",
  },
];
