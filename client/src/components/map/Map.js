import React, { useState } from "react";
import ReactMapGl from "react-map-gl";

export default function Map() {
  const [view, setView] = useState({
    latitude: 45,
    longitude: -75,
    zoom: 5,
  });

  let setPos;
  const getLocal = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(setPos, showError);
    } else {
      console.log("Cant find Location");
    }
  };

  setPos = (position) => {
    setView({
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
      zoom: 8,
    });
  };
  function showError(error) {
    switch (error.code) {
      case error.PERMISSION_DENIED:
        alert("User denied the request for Geolocation.");
        break;
      case error.POSITION_UNAVAILABLE:
        alert("Location information is unavailable.");
        break;
      case error.TIMEOUT:
        alert("The request to get user location timed out.");
        break;
      case error.UNKNOWN_ERROR:
        alert("An unknown error occurred.");
        break;
      default:
        alert("An unknown error occurred.");
    }
  }

  getLocal();

  const geoStyle = {
    top: "0",
    right: "0",
    left: "0",
    bottom: "0",
    width: "100%",
    height: "400px",
    margin: "auto",
    marginTop: "7%",
    border: "12px solid lightblue",
    borderRadius: "50px",
    padding: "12px",
    backgroundColor: "lightblue",
  };

  return (
    <ReactMapGl
      {...view}
      mapboxApiAccessToken={
        "pk.eyJ1IjoibWlzc3RpdGFuazMiLCJhIjoiY2trMXdzNDg1MHZrdjJ2bnljN3M4YTYxMSJ9.u7H0O3R21b8QrNisx7QH1Q"
      }
      mapStyle="mapbox://styles/misstitank3/ckk2203x631md17tco4o9rcm8"
      onViewportChange={(view) => {
        setView(view);
      }}
      style={geoStyle}
      width="90%"
      height="500px"
    ></ReactMapGl>
  );
}
