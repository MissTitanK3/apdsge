import React, { useState } from "react";
import { GlobalButton, RegLogWrapper } from "../../Styles";
import { useHistory } from "react-router-dom";
import { createUser } from "../../api/api";

export default function Register() {
  const history = useHistory();

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const data = await createUser(username, email, password);
      console.log(data.message);
      await setMessage(data.message);
    } catch (err) {
      console.log(err.message);
    }
  };

  return (
    <RegLogWrapper>
      <form onSubmit={handleSubmit}>
        <h1>Register</h1>
        <label>
          <input
            type="text"
            value={username}
            name="name"
            id="name"
            placeholder="Name"
            onChange={(e) => setUsername(e.target.value)}
          />
        </label>
        <br />
        <label>
          <input
            type="email"
            value={email}
            name="email"
            id="email"
            placeholder="Email"
            onChange={(e) => setEmail(e.target.value)}
          />
        </label>
        <br />
        <label>
          <input
            type="password"
            value={password}
            name="password"
            id="password"
            placeholder="Password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </label>
        <br />
        <GlobalButton type="submit">Register</GlobalButton>
        <div className="message">{message}</div>
      </form>
      <GlobalButton onClick={() => history.push("./login")}>Login</GlobalButton>
    </RegLogWrapper>
  );
}
