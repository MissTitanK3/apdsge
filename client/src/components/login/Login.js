import React, { useState } from "react";
import { GlobalButton, RegLogWrapper } from "../../Styles";
import { useHistory } from "react-router-dom";
import { loginUser } from "../../api/api";

export default function Login() {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const data = await loginUser(email, password);
      console.log(data);
      await setMessage(data.message);
      localStorage.setItem("token", data.token);
      if (localStorage.getItem("token")) {
        setEmail("");
        setPassword("");
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  return (
    <RegLogWrapper>
      <form onSubmit={handleSubmit}>
        <h1>Login</h1>
        <br />
        <label>
          <input
            type="email"
            value={email}
            name="email"
            id="email"
            placeholder="Email"
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
        </label>
        <br />
        <label>
          <input
            type="password"
            value={password}
            name="password"
            id="password"
            placeholder="Password"
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
        </label>
        <br />
        <GlobalButton type="submit">Login</GlobalButton>
        <div className="error_message">{message}</div>
      </form>
      <GlobalButton onClick={() => history.push("./register")}>Register</GlobalButton>
    </RegLogWrapper>
  );
}
