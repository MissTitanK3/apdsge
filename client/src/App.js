import "./App.css";
import Header from "./components/menus/Header";
import FooterNav from "./components/menus/FooterNav";
import Menu from "./components/menus/Menu";
import Settings from "./components/settings/Settings";
import "./components/menus/GameMenu.css";

function App() {
  return (
    <div className="App">
      <Header />
      <Menu />
      <Settings />
      <FooterNav />
    </div>
  );
}

export default App;
