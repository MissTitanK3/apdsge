import React, { useState, useEffect } from 'react'
import { GameWrapper } from '../../Styles'
import Scanner2 from '../../img/scanner2.png'

export default function Eng() {
  const [time, updateTime] = useState(new Date())

  useEffect(() => {
    let timer = setInterval(() => tick(), 1000)
    return function clean() {
      clearInterval(timer)
    }
  })

  function tick() {
    updateTime(new Date())
  }


  return (
    <GameWrapper>
      <>
        <img src={Scanner2} alt="Scanner" id="scanner" />
        <button id='scan'>Beam Me Up</button>
        <div id="time">{time.toLocaleTimeString()}</div>
        <div id="shipLocal">
          <p>Transporter Active</p>
        </div>

      </>
    </GameWrapper>
  )
}
