import React, { useState, useEffect } from 'react'
import { GameWrapper } from '../../Styles'
import Scanner2 from '../../img/scanner2.png'

export default function Sci() {
  const [time, updateTime] = useState(new Date())
  const [scan, setScan] = useState({
    scanCoords: {
      latitude: null,
      longitute: null,
    },
    resources: {
      wFuel: null,
      iFuel: null,
      reputation: null,
      oRep: null,
      metals: null,
      salvage: {
        reactor: null,
        shieldGen: null,
        weapons: null,
      }
    }
  })

  useEffect(() => {
    let timer = setInterval(() => tick(), 1000)
    return function clean() {
      clearInterval(timer)
    }
  })

  function tick() {
    updateTime(new Date())
  }
  useEffect(() => {
    // eslint-disable-next-line
    let scanning = activeScan()
    // eslint-disable-next-line
  }, [])

  function randNum() {
    return Math.floor(Math.random() * 10)
  }

  function activeScan() {
    setScan({
      scanCoords: {
        latitude: randNum(),
        longitute: randNum(),
      },
      resources: {
        wFuel: randNum(),
        iFuel: randNum(),
        reputation: randNum(),
        oRep: randNum(),
        metals: randNum(),
        salvage: {
          reactor: randNum(),
          shieldGen: randNum(),
          weapons: randNum(),
        }
      }
    })
  }

  return (
    <GameWrapper>
      <div id="scanResults">
        <p>Warp Fuel: {scan.resources.wFuel}</p>
        <p>Impulse Fuel: {scan.resources.iFuel}</p>
        <p>Reputation: {scan.resources.reputation}</p>
        <p>Organizaion Reputation: {scan.resources.oRep}</p>
        <p>Metals: {scan.resources.metals}</p>
        <p>Salvage Reactor: {scan.resources.salvage.reactor}</p>
        <p>Salvage Shield Generator: {scan.resources.salvage.shieldGen}</p>
        <p>Salvage Weapons: {scan.resources.salvage.weapons}</p>
      </div>
      <>
        <img src={Scanner2} alt="Scanner" id="scanner" />
        <button onClick={activeScan} id='scan'>Scan Now</button>
        <div id="time">{time.toLocaleTimeString()}</div>
        <div id="shipLocal">
          <p>Your ship is 10m away</p>
        </div>
      </>
    </GameWrapper>
  )
}
