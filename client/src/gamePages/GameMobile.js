import React, { Component } from 'react'
import Com from './scans/Com';
import Eng from './scans/Eng'
import Sci from './scans/Sci'

export default class GameMobile extends Component {
  state = {
    sci: true,
    eng: false,
    coms: false,
  }

  render() {
    const renderSci = this.state.sci ? <Sci /> : null;
    const renderEng = this.state.eng ? <Eng /> : null;
    const renderComs = this.state.coms ? <Com /> : null;


    return (
      <div>
        {renderSci}
        {renderEng}
        {renderComs}
        <div id="tricorderBtns">
          <button onClick={() => { this.setState({ sci: true, eng: false, coms: false }) }} ></button>
          <button onClick={() => { this.setState({ sci: false, eng: false, coms: true }) }} ></button>
          <button onClick={() => { this.setState({ sci: false, eng: true, coms: false }) }} ></button>
        </div>
      </div>
    )
  }
}
