import React from "react";
import Map from "../components/map/Map";

import { GameWrapper } from "../Styles";
import GameMobile from "./GameMobile";

function GameHome() {
  return (
    <GameWrapper>
      <div id="mobile">
        <GameMobile />
      </div>
      <div id="game">
        <Map />
      </div>
    </GameWrapper>
  );
}

export default GameHome;
