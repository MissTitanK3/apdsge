import styled from "styled-components";
import Background from "./img/bg_4.jpg";
import BlueGeo from "./img/blueGeo.jpg";
import RedGeo from "./img/redGeo.jpg";
import TealGeo from "./img/tealGeo.jpg";
import CaptainView from "./img/captainView.png";
import WhiteGeo from "./img/whiteGeo.jpg";

// Primary Color: #57353E
// Secondary Color: #9E0831
// Terchary Color: #0E189E
// Font Primary Color: #118046
// Font Secondary Color: #4D8568

// Breakpoints
// /* start of desktop styles */

// @media screen and (max-width: 991px) {
//   /* start of large tablet styles */

//   }

//   @media screen and (max-width: 767px) {
//   /* start of medium tablet styles */

//   }

//   @media screen and (max-width: 479px) {
//   /* start of phone styles */

//   }

// Creating Styled Components.

const HomeWrapper = styled.div`
  color: white;
  text-align: center;
  margin: 30px 0;

  h3 {
    background-color: #060b26;
  }

  @media only screen and (min-width: 1290px) {
    height: 100vh;
    background-image: url(${Background});
    background-repeat: no-repeat;
    background-position: center;
    background-size: 100vw;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;

    h2 {
      color: whitesmoke;
      font-size: 5em;
      text-align: center;
    }
    h3 {
      font-size: 3em;
    }
    #homeStartBtn {
      text-align: center;
      margin: 25px;
    }
    p {
      text-align: center;
      font-size: 2em;
      max-width: 1280px;
      padding: 15px;
    }
  }
`;

const TributeWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 90%;
  justify-content: center;
  p {
    color: black;
    background-image: url(${WhiteGeo});
    background-size: 100%;
  }
`;

const NavWrapper = styled.div`
  width: 100vw;
  display: flex;
img {
  width: 10em;
}
#contactLogo {
display: none;
}



@media only screen and (min-width: 1290px) {
width: 100%;
top: 0;
left: 0;
right: 0;
font-family: StarTrekFont,'Exo', sans-serif;;

#contactLogo {
display: block;
}

.whiteSpace {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 75px;
  border-radius: 3px;
  background: #102140;
  z-index: -1;
}
ul {
  
  background: rgb(58,118,232);
background: linear-gradient(180deg, rgba(58,118,232,1) 8%, rgba(255,255,255,1) 48%, rgba(58,118,232,1) 100%);
  list-style: none;
  border-radius: 3px;
  align-items: center;
  width: 100%auto;
  border: 2px solid #3A76E8;
  box-shadow: 2px 2px 2px #3A76E8;
  img {
      display: inline-block;
    }
  li {
    padding-bottom: 5px;
      width: 100%;
      height: 100%;
      a {
        display: block;
        color: black;
        text-decoration: none;
        text-align: center;
          &:hover {
            background-color: rgb(58,118,232);
            color: #0E189E;
            width: 100%;
        }
      }
    }
  }
}

display: flex;
align-items: center;
justify-content: center;

.whiteSpace {
position: fixed;
top: 0;
left: 0;
width: 100%;
height: 52px;
border-radius: 3px;
background: #aeadaa;
z-index: -1;
}
ul {
  height: 52px;
  display: flex;
  justify-content: space-around;
  img {
    display: unset;
    width: 110px;
  }
  a {
    display: block;
    padding: 15px 10px;
    &:hover {
      background-color: rgb(58,118,232);
      color: #0E189E;
      width: 100%;
    }
  }
}
}

`;

const RegLogWrapper = styled.div`
  display: flex;
  height: 100vh;
  form {
    background-color: silver;
    margin: auto;
    flex-direction: column;
    text-align: center;
    border: 2px solid black;
    border-radius: 5px;
    height: 150px;
    width: 400px;
    margin-top: 50px;
    label {
      background-color: silver;
      input {
        margin: 3px;
      }
    }
    button {
      height: 25px;
      width: 45%;
      margin: 5px;
    }
  }
`;

const AboutWrapper = styled.div`
  color: white;
  text-align: center;
  margin: 40px 0;

  .rebecca {
    background-color: rebeccapurple;
  }

  h2 {
    color: whitesmoke;
    background-color: #060b26;
  }

  h1 {
    background-color: teal;
  }

  ul {
    margin-top: 30px;
  }

  @media only screen and (min-width: 1290px) {
    width: 100vw;
    color: black;
    padding-top: 25px;
    height: 100%;
    background-color: black;
    display: flex;
    flex-direction: column;

    h1 {
      text-align: center;
    }
    #about {
      text-align: center;
    }
    #features {
      ul {
        display: flex;
        flex-direction: column;
        list-style: none;
        margin: 10px;
        li {
          padding: 15px;
        }
      }
    }
    display: flex;
    justify-content: center;
    text-align: center;
    font-size: 1.5em;
    p {
      max-width: 1280px;
      margin: auto;
    }
    #about {
      background-image: url(${TealGeo});
      background-repeat: no-repeat;
      background-position: center;
      height: 450px;
      justify-content: center;
      align-self: center;
    }

    #features-right {
      width: 100%;
      margin: auto;
      background-image: url(${BlueGeo});
      background-repeat: no-repeat;
      background-position: center;
      background-size: 100vw;
      h1 {
        background-color: rgb(0, 132, 215);
      }

      ul {
        width: 40%;
        height: 450px;
        border: 2px solid black;
        box-shadow: 2px 2px 2px black;
        background-color: silver;
        li {
          text-align: center;
          padding: 25px;
          list-style: none;
        }
      }
    }
    #features-left {
      width: 100%;
      margin: auto;
      background-image: url(${RedGeo});
      background-repeat: no-repeat;
      background-position: center;
      background-size: 100vw;
      h1 {
        background-color: rgb(108, 33, 66);
      }

      div {
        display: flex;
        justify-content: flex-end;
        ul {
          width: 40%;
          height: 450px;
          border: 2px solid black;
          box-shadow: -2px -2px 2px black;
          background-color: silver;
          li {
            text-align: center;
            padding: 25px;
            list-style: none;
          }
        }
      }
    }
  }
`;
const GlobalButton = styled.button`
    justify-content: center;
    height: 25px;
    width: 45%;
    margin: 5px;
    margin: auto;
  }
`;
const HomeButton = styled.button`
    justify-content: center;
    height: 60px;
    width: 100%;
    margin: 5px;
    margin: auto;
    font-size: 3em;
  }
`;

const FooterWrapper = styled.div`
  #Footer{
    width: 100%;
    height: 100vh
    position:relative;
    bottom: 0;
    background-color: silver;
    text-align: center;
  }
  ul{
  list-style: none;
  align-items: center;
  width: 100%auto;
  display: flex;
    li {
      width: 100%;
      height: 100%;
      a {
        color: #0E189E;
        text-decoration: none;
        text-align: center;
        &:hover {
          color: #4D8568;
          width: 100%;
        }
      }
    }
  }
}
`;

const OurTeamWrapper = styled.div`
  align-self: center;
  width: 250px;
  padding: 10px;
  display: flex;
  margin: auto;
  flex-flow: column wrap;
  section {
    border: 2px solid black;
    box-shadow: 2px 2px 2px black;
    justify-content: center;
    height: 100%;
    padding: 10px;
    color: black;
    border-top-right-radius: 20%;
    background-color: white;
    width: 280px;
    img {
      width: 100%;
    }
  }
`;

const TeamWrapper = styled.div`
  display: flex;
  background-color: black;
  align-content: left;
  width: 100 %;
  @media only screen and (min-width: 1290px) {
    div {
      justify-content: center;
      display: flex;
      flex-wrap: wrap;
      margin: 50px;
    }
  }
`;

const DashboardWrapper = styled.div`
  #tabPanel {
    height: 30px;
    padding: 5px;
    margin-bottom: 10px;

    ul {
      list-style: none;
      align-items: stretch;
      width: 100%auto;
      display: flex;
      li {
        border-right: 1px solid black;
        width: 100%;
        height: 100%;
        a {
          color: white;
          text-align: center;
          text-decoration: none;
          &:hover {
            color: black;
          }
        }
      }
    }

    border-bottom: 2px solid rgb(51, 153, 255);
    background: rgb(204, 204, 255, 0.5);
  }
  #topContainer {
    justify-content: space-around;
    display: flex;
    height: 250px;
    width: 100 %;
    background: rgb(204, 204, 255, 0.5);
    margin: auto;
    margin-bottom: 10px;
  }
  #profilePic {
    border-radius: 2000px;
    background: white;
    width: 150px;
    height: 150px;
    display: inline-block;
    margin-left: 20px;
    margin-top: 50px;
  }
  #aboutContainer {
    border-radius: 15px 50px 30px 5px;
    border: 2px solid rgb(51, 153, 255);
    background: #f2f3f4;
    height: 150px;
    width: 45%;
    margin-top: 50px;
  }
  #aboutPar {
    padding: 5px;
    font-size: 15px;
  }
  #nameContainer {
    display: inline-block;
    margin-top: 100px;
    margin-left: 20px;
    color: white;
  }
  .flName {
    text-align: center;
  }
  #feed {
    padding: 20px;
    border-radius: 80px 0px 80px 0px;
    margin-bottom: 10px;
    margin-right: auto;
    margin-left: auto;
    height: 100 %;
    background: rgb(204, 204, 255, 0.5);
    color: white;
  }
  #feederName {
    padding: 10px;
    border-bottom: 2px solid rgb(51, 153, 255);
  }
  #news1 {
    margin: auto;
    height: 90 %;
    width: 100 %;
    background: f2f3f4;
  }
`;
const MessagesWrapper = styled.div`
  #tabPanel {
    height: 30px;
    padding: 5px;

    ul {
      list-style: none;
      align-items: stretch;
      width: 100%auto;
      display: flex;
      li {
        border-right: 1px solid black;
        width: 100%;
        height: 100%;
        a {
          color: white;
          text-align: center;
          text-decoration: none;
          &:hover {
            color: black;
          }
        }
      }
    }

    border-bottom: 2px solid rgb(51, 153, 255);
    background: rgb(204, 204, 255, 0.5);
  }

  .react-tabs__tab {
    padding-right: 48px;
    padding-left: 48px;
    width: 25%;
    text-align: center;
    color: white;
    z-index: auto;
  }

  .react-tabs__tab--selected {
    width: 25%;
    color: black;
    text-align: center;
  }

  #msgTabs {
    border-bottom: 2px solid rgb(51, 153, 255);
    background: rgb(204, 204, 255, 0.5);
    justify-content: space-around;
    display: flex;
    margin-bottom: 25px;
  }

  #inboxContainer {
    margin: auto;
    height: 60vh;
    width: 95%;
    background: rgb(204, 204, 255, 0.5);
    padding: 30px;
  }

  #inbox {
    margin: auto;
    height: 100%;
    width: 100%;
    background: white;
  }
  #emailList {
    height: 40vh;
    list-style: none;
    overflow-y: scroll;
    padding-left: 20px;
    li {
      padding: 2px;
      border-bottom: 1px solid rgb(51, 153, 255);
    }
  }
  #draftContainer {
    margin: auto;
    height: 60vh;
    width: 95%;
    background: rgb(204, 204, 255, 0.5);
    padding: 30px;
  }

  #draft {
    margin: auto;
    height: 100%;
    width: 100%;
    background: white;
  }
  #draftList {
    height: 40vh;
    list-style: none;
    overflow-y: scroll;
    padding-left: 20px;
    li {
      padding: 2px;
      border-bottom: 1px solid rgb(51, 153, 255);
    }
  }

  #sentContainer {
    margin: auto;
    height: 60vh;
    width: 95%;
    background: rgb(204, 204, 255, 0.5);
    padding: 30px;
  }

  #sent {
    margin: auto;
    height: 100%;
    width: 100%;
    background: white;
  }
  #sentList {
    height: 40vh;
    list-style: none;
    overflow-y: scroll;
    padding-left: 20px;
    li {
      padding: 2px;
      border-bottom: 1px solid rgb(51, 153, 255);
    }
  }
  #deleteCheckbox {
    margin: 5px;
  }
`;

const FriendsWrapper = styled.div`
  #tabPanel {
    margin-bottom: 20px;
    height: 30px;
    padding: 5px;

    ul {
      list-style: none;
      align-items: stretch;
      width: 100%auto;
      display: flex;
      li {
        border-right: 1px solid black;
        width: 100%;
        height: 100%;
        a {
          color: white;
          text-align: center;
          text-decoration: none;
          &:hover {
            color: black;
          }
        }
      }
    }

    border-bottom: 2px solid rgb(51, 153, 255);
    background: rgb(204, 204, 255, 0.5);
  }

  input[type="text"] {
    margin-left: 10%;
    width: 80%;
    box-sizing: border-box;
    border: 1px solid rgb(51, 153, 255);
    border-radius: 4px;
    font-size: 12px;
    background-color: white;
    background-position: 10px 10px;
    background-repeat: no-repeat;
    padding: 5px 10px 5px 10px;
  }
  #friendsContainer {
    margin: auto;
    height: 65vh;
    width: 95%;
    background: rgb(204, 204, 255, 0.5);
    padding: 30px;
  }

  #friends {
    margin: auto;
    height: 80%;
    width: 100%;
    background: white;
    margin-top: 20px;
  }
  #friendsList {
    height: 40vh;
    list-style: none;
    overflow-y: scroll;
    padding-left: 20px;
    li {
      padding: 2px;
      border-bottom: 1px solid rgb(51, 153, 255);
    }
  }
`;

const CharactersWrapper = styled.div`
  #tabPanel {
    margin-bottom: 20px;
    height: 30px;
    padding: 5px;

    ul {
      list-style: none;
      align-items: stretch;
      width: 100%auto;
      display: flex;
      li {
        border-right: 1px solid black;
        width: 100%;
        height: 100%;
        a {
          color: white;
          text-align: center;
          text-decoration: none;
          &:hover {
            color: black;
          }
        }
      }
    }

    border-bottom: 2px solid rgb(51, 153, 255);
    background: rgb(204, 204, 255, 0.5);
  }

  #characterContainer {
    margin: auto;
    height: 65vh;
    width: 95%;
    background: rgb(204, 204, 255, 0.5);
    padding: 30px;
  }
`;

const ContactWrapper = styled.div`
  #contactLogoContainer {
    padding-top: 20px;
    justify-content: center;
    display: flex;
    width: 100%;
    height: 200px;
    #contCloseLogo {
      height: 70%;
    }
    #contactLogo {
      height: 70%;
    }
    #contSpaceLogo {
      height: 70%;
    }
  }
  #pageContainer {
    margin-bottom: 20px;
    margin: auto;
    width: 80%;
    height: 60vh;
  }
  #contactContainer {
    padding: 40px;
    border-radius: 80px 0px 80px 0px;
    width: 100%;
    height: 400px;
    background: rgb(204, 204, 255, 0.5);
    justify-content: center;
    display: flex;
  }

  #contactForm {
    display: inline-block;
    width: 50%;
    form {
      margin-top: 40px;
      input {
        flex-direction: column;
      }
    }
  }
  h4 {
    margin-bottom: 20px;
  }
  #address {
    display: inline-block;
  }
`;

const GameWrapper = styled.div`
  #game {
    display: none;
  }
  #scanner {
    display: flex;
    height: 100vh;
    width: 100vw;
    max-height: 640px;
    max-width: 360px;
    margin: auto;
    z-index: -20;
    border: 12px solid black;
    box-shadow: 13px 13px 9px black;
    border-radius: 15px;
  }
  #scan {
    display: flex;
    z-index: 2;
    position: relative;
    width: 250px;
    height: 50px;
    top: -475px;
    left: 5px;
    justify-content: center;
    vertical-align: center;
    margin: auto;
    background-color: rgba(153, 153, 255, 0.7);
    font-size: 2em;
    font-weight: bold;
    border-radius: 15px;
  }
  #scanResults {
    display: flex;
    flex-direction: column;
    margin: auto;
    text-align: center;
    background-color: rgb(153, 153, 255);
    max-width: 360px;
    border: 12px solid black;
    box-shadow: 13px 13px 9px black;
    border-radius: 15px 15px 0 0;
    padding: 5px;
    margin-top: 15px;
  }
  #time {
    display: flex;
    z-index: 2;
    position: relative;
    width: 250px;
    height: 50px;
    top: -320px;
    left: 5px;
    justify-content: center;
    vertical-align: center;
    margin: auto;
    color: rgb(153, 153, 255);
    font-size: 2em;
    font-weight: bold;
  }

  #shipLocal {
    display: flex;
    z-index: 2;
    position: relative;
    width: 250px;
    height: 50px;
    top: -300px;
    left: 5px;
    justify-content: center;
    vertical-align: center;
    margin: auto;
    color: rgb(153, 153, 255);
    font-size: 1em;
  }
  #tricorderBtns {
    display: flex;
    z-index: 2;
    position: relative;
    top: -285px;
    justify-content: center;
    vertical-align: center;
    margin: auto;
    color: rgb(153, 153, 255);
    font-size: 1em;
    button {
      width: 90px;
      height: 75px;
      background-color: transparent;
      &:active {
        border: rgba(153, 153, 255, 0.7);
      }
    }
  }

  @media only screen and (min-width: 1290px) {
    #game {
      display: flex;
    }
    #mobile {
      display: none;
    }
    margin: auto;
    height: 95vh;
    width: 100vw;
    background-image: url(${CaptainView});
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
  }
`;

// Must Export to use in app.
export {
  HomeWrapper,
  NavWrapper,
  RegLogWrapper,
  AboutWrapper,
  GlobalButton,
  FooterWrapper,
  DashboardWrapper,
  OurTeamWrapper,
  TeamWrapper,
  ContactWrapper,
  HomeButton,
  GameWrapper,
  MessagesWrapper,
  FriendsWrapper,
  CharactersWrapper,
  TributeWrapper,
};

// Import Components that you need on the page.
