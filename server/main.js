// ESM syntax is supported.
import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import cors from "cors";
const dotenv = require("dotenv");
const path = require("path");

//get db from dotenv
dotenv.config();
// creating port variable
const port = process.env.PORT || 4000;

const app = express();

//middleware
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ extended: false }));

//import routes
const authRoute = require("./routes/authentication");
const charRoute = require("./routes/character");
const newsRoute = require("./routes/newsFeed");
const dashRoute = require("./routes/dashboard");
const messageRoute = require("./routes/messages");
//use route as middleware
app.use(cors());
app.use("/user", authRoute);
app.use("/character", charRoute);
app.use("/newsfeed", newsRoute);
app.use("/dashboard", dashRoute);
app.use("/messages", messageRoute);
//connect to DB (using config file)
const connectToDB = async () => {
  try {
    mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true }, () => {
      console.log("Connected to DB");
    });
  } catch (err) {
    console.log(err.message);
  }
};

//Server Static assets

app.get("/", (req, res) => {
  res.json("this get request is working");
});

//set up listen
app.listen(port, () => {
  connectToDB();
  console.log(`Now listening to ${port} `);
});
