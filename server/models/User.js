import mongoose from "mongoose";

//User Schema
const Schema = mongoose.Schema;

const UserSchema = Schema({
  username: {
    type: String,
    trim: true,
    required: true,
  },
  email: {
    type: String,
    trim: true,
    required: true,
  },
  password: {
    type: String,
    trim: true,
    required: true,
  },
  ppic: {
    type: String,
    required: false,
  },
  description: {
    type: String,
    required: false,
  },
  messages: {
    type: Array,
    required: false,
  },
  friends: {
    type: Array,
    required: false,
  },
});

module.exports = mongoose.model("User", UserSchema);
