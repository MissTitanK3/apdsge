import mongoose from "mongoose";

const schema = mongoose.Schema;

const NewsFeed = schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  date: {
    type: String,
  },
  author: {
    type: String,
  },
});

module.exports = mongoose.model("NewsFeed", NewsFeed);
