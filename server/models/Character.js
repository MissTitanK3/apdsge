import mongoose from "mongoose";

const Schema = mongoose.Schema;

const CharacterSchema = Schema({
  name: {
    type: String,
    required: true,
  },
  group: {
    type: String,
    default: "starfleet",
  },
  color: {
    type: String,
    required: true,
  },
  selected: {
    type: Boolean,
  },
  creator: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Character", CharacterSchema);
