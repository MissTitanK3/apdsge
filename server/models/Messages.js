import mongoose from "mongoose";

const Schema = mongoose.Schema;

const MessagesSchema = Schema({
  name: {
    type: String,
    required: true,
  },
  user_id: {
    type: String,
    required: true,
  },
  messages: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Messages", MessagesSchema);
