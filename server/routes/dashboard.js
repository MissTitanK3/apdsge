const router = require("express").Router();
const User = require("../models/User");
const verifyToken = require("./verifyToken");

router.patch("/", verifyToken, async (req, res) => {
  try {
    const user = await User.findOneAndUpdate(
      { _id: req.user._id },
      { description: req.body.description },
      { new: true },
    );
    console.log(user);
    res.json(user);
  } catch (err) {
    res.send(err.message);
  }
});

module.exports = router;
