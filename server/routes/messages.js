const router = require("express").Router();
const verify = require("./verifyToken");
const Message = require("../models/Messages");
const User = require("../models/User");

router.post("/", verify, async (req, res) => {
  const id = req.user._id.toString();
  const user = await User.findOne({ _id: id });
  console.log(user);
  const message = new Message({
    name: user.username,
    messages: req.body.messages,
    user_id: req.user._id,
  });
  console.log(message);
  try {
    const savedmessage = await message.save();
    res.json(savedmessage);
  } catch (err) {
    console.log(err.message);
  }
});

router.get("/", verify, async (req, res) => {
  console.log(req.user._id);
  const id = req.user._id.toString();
  try {
    const messages = await Message.find({ user_id: id });
    res.json(messages);
  } catch (err) {
    console.log(err.message);
  }
});

module.exports = router;
