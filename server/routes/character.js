const router = require("express").Router();
const verify = require("./verifyToken");
const Char = require("../models/Character");

//character post request
router.post("/", verify, async (req, res) => {
  // res.send(req.user._id);

  const char = new Char({
    name: req.body.name,
    group: req.body.group,
    creator: req.user._id,
    color: req.body.color,
  });
  try {
    const savedChar = await char.save();
    res.status(200).json(savedChar);
  } catch (err) {
    res.status(400).send(err.message);
  }
});

//character get request
router.get("/", verify, async (req, res) => {
  try {
    const chars = await Char.find({ creator: req.user._id });
    res.json(chars);
  } catch (err) {
    res.status(400).send(err.message);
  }
});

module.exports = router;
