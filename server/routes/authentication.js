const router = require("express").Router();
const User = require("../models/User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const axios = require("axios");
const md5 = require("md5");
const verifyToken = require("./verifyToken");
const { registerValidation, loginValidation } = require("../validation/Validation");

// test gravatar email
// tianna.m.mccoy@gmail.com

router.post("/register", async (req, res) => {
  const { error } = registerValidation(req.body);
  if (error) return res.send({ message: error.details[0].message });

  //checking if user is already in the db
  const emailExist = await User.findOne({ email: req.body.email });
  if (emailExist) return res.send({ message: "Email already exists" });

  const userExists = await User.findOne({ username: req.body.username });
  if (userExists) return res.send({ message: "Username taken" });

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);

  const hashedEmail = md5(req.body.email.toLowerCase().trim());
  console.log(hashedEmail);

  // const { data } = await axios.get(`https://www.gravatar.com/avatar/${hashedEmail}`);
  // console.log(data);

  const user = new User({
    username: req.body.username,
    email: req.body.email,
    password: hashedPassword,
    ppic: `https://www.gravatar.com/avatar/${hashedEmail}`,
    description: "",
    messages: [],
    friends: [],
  });
  try {
    const savedUser = await user.save();
    res.status(200).send({ message: "You've been regiSTAARRED" });
  } catch (err) {
    res.status(400).send(err.message);
  }
});

router.post("/login", async (req, res) => {
  const { error } = loginValidation(req.body);
  if (error) return res.send({ message: error.details[0].message });

  //check to see if user exists
  const user = await User.findOne({ email: req.body.email });
  if (!user) return res.send({ message: "Invalid Email" });

  //check if password is correct
  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.send({ message: "Invalid Password" });

  //create a token if logged in and assign a token
  const token = jwt.sign({ _id: user.id }, process.env.TOKEN);
  console.log(user);
  //add to header to so user can access private routes
  res.send({
    token: token,
    message: "Logged In",
  });
});

router.get("/", verifyToken, async (req, res) => {
  console.log(req.headers);
  try {
    const id = req.user._id.toString();
    console.log(id);
    const user = await User.findOne({ _id: id });
    res.json(user);
  } catch (err) {
    console.log(err.message);
    res.send(err.message);
  }
});

module.exports = router;
