const router = require("express").Router();
const verify = require("./verifyToken");
const News = require("../models/NewsFeed");

router.post("/", async (req, res) => {
  let today = new Date();
  const dd = String(today.getDate()).padStart(2, "0");
  const mm = String(today.getMonth() + 1).padStart(2, "0");
  const yyyy = today.getFullYear();
  today = mm + "/" + dd + "/" + yyyy;
  console.log(today);
  const newspost = new News({
    title: req.body.title,
    description: req.body.description,
    author: req.body.author,
    date: today,
  });

  try {
    const savednewspost = await newspost.save();
    res.json(savednewspost);
  } catch (err) {
    console.log(err.message);
  }
});

router.get("/", async (req, res) => {
  try {
    const news = await News.find();
    console.log(news);
    res.json(news);
  } catch (err) {
    console.log(err.message);
    res.send(err.message);
  }
});

module.exports = router;
